<?php
$mail_ontv = 'info@meetelias.com'; 
// Speciale checks voor naam en e-mailadres
if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
    // naam controle
    if (empty($_POST['naam']))
        $naam_fout = 1;
    // e-mail controle
    if (function_exists('filter_var') && !filter_var($_POST['mail'], FILTER_VALIDATE_EMAIL))
        $email_fout = 1;
    // antiflood controle
    if (!empty($_SESSION['antiflood']))
    {
        $seconde = 20; // 20 seconden voordat dezelfde persoon nog een keer een e-mail mag versturen
        $tijd = time() - $_SESSION['antiflood'];
        if($tijd < $seconde)
            $antiflood = 1;
    }
}

// Kijk of alle velden zijn ingevuld - naam mag alleen uit letters bestaan en het e-mailadres moet juist zijn
if (($_SERVER['REQUEST_METHOD'] == 'POST' && (!empty($antiflood) || empty($_POST['naam']) || !empty($naam_fout) ||  empty($_POST['mail']) || !empty($email_fout) || empty($_POST['bericht']) || empty($_POST['onderwerp']))) || !empty($onderwerp_fout) || $_SERVER['REQUEST_METHOD'] == 'GET')
{
    
        
  // HTML e-mail formlier
  echo '<form method="post" action="' . $_SERVER['REQUEST_URI'] . '" />
  <div>
    <label>Naam:</label>
    <input class="inputField L" type="text" id="naam" name="naam" value="' . (isset($_POST['naam']) ? htmlspecialchars($_POST['naam']) : '') . '" />
  </div>

  <div>
    <label>E-mail:</label>
    <input class="inputField M"  type="text" id="mail" name="mail" value="' . (isset($_POST['mail']) ? htmlspecialchars($_POST['mail']) : '') . '" />
  </div>

    <div>
    <label>Onderwerp:</label>
    <input class="inputField S" type="text" id="onderwerp" name="onderwerp" value="' . (isset($_POST['onderwerp']) ? htmlspecialchars($_POST['onderwerp']) : '') . '" />
  </div>
      <div>
    <label>Bericht:</label>
<textarea id="bericht" name="bericht" class="textArea" >' . (isset($_POST['bericht']) ? htmlspecialchars($_POST['bericht']) : '') . '</textarea>
  </div>

      <button type="submit" name="submit" class="noUniform sendbtn">Versturen</button>
      <!--<input class="noUniform sendbtn" type="submit" name="submit" value=" Versturen " />-->

  </form>';if ($_SERVER['REQUEST_METHOD'] == 'POST')
    {
        if (!empty($naam_fout))
            echo '<div class="error"><span>Uw naam is niet ingevuld!</span></div>';
        elseif (!empty($email_fout))
            echo '<div class="error"><span>Uw e-mailadres is niet juist!</span></div>';
        elseif (!empty($antiflood))
            echo '<div class="error"><span>U mag slechts &eacute;&eacute;n bericht per ' . $seconde . ' seconde versturen.</span></div>';
        elseif (!empty($onderwerp_fout))
            echo '<div class="error"><span>U bent het onderwerp vergeten in te vullen.</span></div>';
        else
            echo '<div class="error"><span>U bent het bericht vergeten in te vullen.</span></div>';
    }
}

// versturen naar
else
{      
  // set datum
  $datum = date('d/m/Y H:i:s');
    
  $inhoud_mail = "===================================================\n";
  $inhoud_mail .= "Ingevulde contact formulier " . $_SERVER['HTTP_HOST'] . "\n";
  $inhoud_mail .= "===================================================\n\n";
  
  $inhoud_mail .= "Naam: " . htmlspecialchars($_POST['naam']) . "\n";
  $inhoud_mail .= "E-mail adres: " . htmlspecialchars($_POST['mail']) . "\n";
  $inhoud_mail .= "Bericht:\n";
  $inhoud_mail .= htmlspecialchars($_POST['bericht']) . "\n\n";
    
  $inhoud_mail .= "Verstuurd op " . $datum . " via het IP adres " . $_SERVER['REMOTE_ADDR'] . "\n\n";
    
  $inhoud_mail .= "===================================================\n\n";
  
  // --------------------
  // spambot protectie
  // ------
  // van de tutorial: http://www.phphulp.nl/php/tutorial/beveiliging/spam-vrije-contact-formulieren/340/
  // ------
  
  $headers = 'From: ' . htmlspecialchars($_POST['naam']) . ' <' . $_POST['mail'] . '>';
  
  $headers = stripslashes($headers);
  $headers = str_replace('\n', '', $headers); // Verwijder \n
  $headers = str_replace('\r', '', $headers); // Verwijder \r
  $headers = str_replace("\"", "\\\"", str_replace("\\", "\\\\", $headers)); // Slashes van quotes
  
  $_POST['onderwerp'] = str_replace('\n', '', $_POST['onderwerp']); // Verwijder \n
  $_POST['onderwerp'] = str_replace('\r', '', $_POST['onderwerp']); // Verwijder \r
  $_POST['onderwerp'] = str_replace("\"", "\\\"", str_replace("\\", "\\\\", $_POST['onderwerp'])); // Slashes van quotes
  
  if (mail($mail_ontv, $_POST['onderwerp'], $inhoud_mail, $headers))
  {
      // zorg ervoor dat dezelfde persoon niet kan spammen
      $_SESSION['antiflood'] = time();
      
      echo '<p>Het contactformulier is verzonden</p>
      
      <p>Bedankt voor het invullen van het contactformulier. We zullen zo spoedig mogelijk contact met u opnemen.</p>';
  }
  else
  {
      echo '<p>Het contactformulier is niet verzonden</p>
      
      <p>Onze excuses. Het contactformulier kon niet verzonden worden.</p>';
  }
}
?>