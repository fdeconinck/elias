<!doctype html>
<!--[if lt IE 7]> <html class="no-js ie6 ie" lang="nl"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 ie" lang="nl"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 ie" lang="nl"> <![endif]-->
<!--[if IE 9 ]>   <html class="no-js ie9 ie" lang="nl"> <![endif]-->  
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="nl" class="no-js"> <!--<![endif]--><head>
	<meta charset="utf-8">
    <meta name="description" content="Je kan Elias telefonisch of per e-mail bereiken om een afspraak te maken om elkaar te zien in binnen- of buitenland. Je kan ook het contactformulier invullen. Hij geeft binnen 24 uur antwoord.">
    <meta name="keywords" content="contact, contacteer Elias, contacteer me, bevestiging, bevestig, bel, mail, e-mail, GSM, telefoon, afspraak, genotsmoment, contactformulier, gigolo, gezelschapsheer, voor vrouwen, vrouwen, voor dames, dames">
    <meta name="author" content="">
    <title>Elias - contact</title>
    <link rel="shortcut icon" href="../images/favicon.ico">

    
    <!-- CSS : implied media="all" -->
	<link rel="stylesheet/less" href="../css/style.less">
    
    <script src="../js/less-1.3.0.min.js"></script>
    <!-- Modernizr enables HTML5 elements & feature detects;
    Respond is a polyfill for min/max-width CSS3 Media Queries -->
	<script src="../js/modernizr-2.5.3.min.js"></script>
</head>

<body class="contact">
         <img src="../images/bck_contact.jpg" id="bg" alt="">
         <div id="container">
        <header id="header">
            <div class="wrapper">
                <div id="brand">
                    <a href="../index.html"><img src="../images/lgo_elias.png" width="94" height="95" alt="Elias" title="Elias"></a>
                </div>
                <nav id="nav">
                    <div class="navList">
                        <ul class="sf-menu">
            <li><a href="../index.html">Welkom</a></li>
            <li><a href="content-about.html">Ontmoet me</a>
              <ul class="arrow_box">
                <li><a href="content-about.html">Ontmoet me</a></li>
                <li><a href="content-suggestions.html">Enkele suggesties</a></li>
                <li><a href="content-fantasy.html">Stoute fantasieën</a></li>
              </ul>
            </li>
            <li><a href="content-inbeeld.html">Elias in beeld</a></li>
            <li><a href="content-rates.html">Tarieven</a></li>
            <!--<li><a href="#">Bekentenissen</a></li>-->
            <li><a href="content-faq.html">Vraagbaak</a></li>
          </ul>
                    </div>
                </nav>
                <div class="floatR">
                    <a href="contact.php" class="contactbtn arr">Contacteer me</a>
                </div>
                <nav id="language">
                    <div id="navList">
                        <ul>
                            <li><a href="#">nl</a></li>
                            <li><a href="#" class="noActive">en</a></li>
                        </ul>
                    </div>
                </nav>
            </div>
        </header>
        <div id="main" role="main">
            <div id="content-wrapper" class="clearfix">         
                <section id="content">
                    <div class="block1">
                        <div class="box">
                            <h1>Contact</h1>
                            <blockquote>”Tijd voor je <b>eigen verhaal</b>”</blockquote>
                            <p>Droom je weg bij de verhalen die je hier leest?<br>Het hoeft niet bij dromen te blijven...<br> Weet dat je het waard bent, en schenk jezelf dit genotsmoment.</p>
                        </div>
                        <div class="box"><p>Neem je agenda en bel of mail me, als eerste stap naar een bijzondere ervaring, voor jou en mij.<br>
We kunnen elkaar zien, ergens in België en ver daar buiten. Binnen de 24 uur bevestig ik jou onze afspraak.
</p><img src="../images/img_cards.png" alt="Elias in beeld" usemap="#Map" title="Elias in beeld">
<map name="Map">
  <area shape="rect" coords="5,97,228,144" href="mailto:info@meetelias.com" target="_blank" alt="Contacteer me: info@meetelias.com" title="Contacteer me: info@meetelias.com">
</map>
                      </div>
                </div>
                <div class="block2">
                    <div class="box">
                       <?php include 'mail.php'; ?>
                    </div>
                </div>
                </section>
            </div>
        </div>
        <div id="push"></div>
    </div>
    <footer id="footer">
        <div id="foot-list">
            <div class="floatL">
                &copy; Copyright Meet-Elias 2013        
            </div>
            <div class="floatR">
                <ul>
                    <li><b>Gsm</b>  +32(0)483 622 692</li>
                    <li><b>e-mail</b>  <a href="mailto:info@meetelias.com">info@meetelias.com</a></li>
                </ul>
            </div>
        </div>
    </footer>
    <!-- Javascript at the bottom for fast page loading -->
    <script src="../js/jquery-1.7.1.min.js"></script>
    <script src="../js/superfish.js"></script> 
    
    <!-- Custom form styling plugin -->
    <script src="../js/jquery.uniform.js"></script>
    <script src="../js/jquery.masonry.min.js"></script> 
<!-- Add fancyBox main JS and CSS files --> 
<script type="text/javascript" src="../js/jquery.fancybox.js"></script> 
    
    <!-- scripts concatenated and minified via ant build script -->
    <script src="../js/script.js"></script>
    <!-- end scripts -->
    
</body>
</html>
