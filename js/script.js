/* Author:

*/
	/*
		INIT
	*/


	$(function() {   
		
		var theWindow        = $(window),
		$bg              = $("img#bg"),
		aspectRatio      = $bg.width() / $bg.height();
			    			    		
		function resizeBg() {
				
			if ( (theWindow.width() / theWindow.height()) < aspectRatio ) {
			    $bg
			    	.removeClass()
			    	.addClass('bgheight');
			} else {
			    $bg
			    	.removeClass()
			    	.addClass('bgwidth');
			}
							
		}
			                   			
		theWindow.resize(function() {
			resizeBg();
		}).trigger("resize");
			
	});


	$(window).resize(function() {
	// Fix the background
		bg_resize();
	}); 



	$(document).ready(function(e)
	{
				$('img#bg').fadeIn(750);
 	
		$('.fancybox').fancybox();
		bg_load();
		// Custom form elements
		// When ajax is used to load new form elements, call this: $("input, textarea, select, button").uniform(); to execute the plugin for the new elements
		var selector_uniform = '.noUniform'; 
        if($('html').hasClass('ie') == true){ selector_uniform += ',input[type=file]'; } // ie 8 en 9 file upload is niet stijlbaar, door submit bug
        $("input, textarea, select, button").not(selector_uniform).uniform();
        $('ul.sf-menu').superfish();
		$('.twocolscontent').columnize({ columns: 2 });
		/*$('.threecolscontent').columnize({ columns: 3 });*/
		var highestCol = Math.max($('.wrapper .block').height());
		$('.wrapper .block').height(highestCol);
		
		var $container = $('.portfolio-content');

		$container.imagesLoaded( function(){
		  $container.masonry({
	   		itemSelector : '.box',
			columnWidth: 1
		 });
		});
	});

	var bg_images = new Array(
		'images/bck_elias1.jpg',			
		'images/bck_elias2.jpg',			
		'images/bck_elias3.jpg'			
	);

	function bg_load() {
	// load bg
	// Set random background
	var bg_image = bg_images[Math.floor(Math.random()*bg_images.length)];
	// add image to bg if empty
	if($('div#bg img').length == 0) {
		$('div#bg').append('<img />');
	}
	$('div#bg img').attr('src', bg_image);
	// Preload background image
	var cache = [];
	var cacheImage = document.createElement('img');
	cacheImage.src = $('div#bg img').attr('src');
	cache.push(cacheImage);
	// Reveal background
	$('div#bg img').load(function() {
		// fade in
		$('div#bg').fadeIn(750);
		// resize bg
		bg_resize();
	});
	// Still is not loaded?
	setTimeout(function() {
		// resize first
		$('div#bg').fadeIn(750);
			bg_resize();
		}, 2500);
	}
function bg_resize() {
//
// resize bg to fit the window
// set background height = window height
$('div#bg').height($(window).height());
//
$('div#bg img').each(function(){
var ratio = $(this).width()/$(this).height();
// if bg width is smaller than window width
if($(this).width() < $(window).width()) {
$(this).width($(window).width());
$(this).height($(this).width()/ratio)
}
// if bg height is smaller than window height
if($(this).height() < $(window).height()) {
$(this).height($(window).height());
$(this).width($(this).height()*ratio)
}
// position to center of the screen
$(this).css({
'margin-top':'0px'
})
// make sure cont has same height as bg
$('#container').height($('div#bg').height());
});
} 
